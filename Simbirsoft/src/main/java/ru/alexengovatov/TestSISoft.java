package ru.alexengovatov;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestSISoft {

    /**
     * @author Alex Engovatov
     * Apr 2021
     * 27 sec
     *
     * Aim: Make authrization and send a message for myself with quantity of messages with theme "Simbersoft Тестовое задание"
     * Steps:
     * 1. Open chromedriver and go to mail.yandex.ru
     * 2. Authorization
     * 3. Search quantity of messages
     * 4. Save quantity of messages into variable
     * 5. Send a message with quantity
*/
    private static final Logger logger = LogManager.getLogger(TestSISoft.class.getSimpleName());

    WebDriver driver;
    WebDriverWait wait;
    @Before
    public void settings() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver.exe");
        driver =new ChromeDriver();

    }
    @After
    public void close(){
        driver.quit();

    }
    @Test
    public void test(){
        logger.info("Start Chrome");
        driver.get("http://mail.yandex.ru");
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div/div/div/div/div/a/span[text()='Войти']")));

        logger.info("Authorization");
        driver.findElement(By.xpath("//div/div/div/div/div/a[@href='https://passport.yandex.ru/auth?from=mail&origin=hostroot_homer_auth_ru&retpath=https%3A%2F%2Fmail.yandex.ru%2F&backpath=https%3A%2F%2Fmail.yandex.ru%3Fnoretpath%3D1'][last()]")).click();
        driver.findElement(By.id("passp-field-login")).sendKeys("engovatovalex");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='Button2 Button2_size_l Button2_view_action Button2_width_max Button2_type_submit']/span[text()='Войти']")));
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("passwd")));
        driver.findElement(By.name("passwd")).sendKeys("2020Ru14@");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("passwd")));
        driver.findElement(By.xpath("//button[@type='submit']")).click();

        logger.info("Searching quantity of messages");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='text']")));
        driver.findElement(By.xpath("//input[@type='text']")).sendKeys("Simbirsoft тестовое задание");
        driver.findElement(By.xpath("//button[@title='расширенный поиск']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mail-MessagesSearchInfo-Title']")));
        driver.findElement(By.xpath("//div[@class='mail-AdvancedSearch']/button[3]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button/span/span[text()='Папки']")));
        driver.findElement(By.xpath("//div/div/span[text()='Входящие']")).click();
        wait.
                until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath("//div[@class='mail-MessagesSearchInfo_Summary'][text()='Результаты поиска «Simbirsoft тестовое задание» в папке «Входящие» ']")));

        logger.info("Save quantity of messages");
        String result = String
                .valueOf(driver.findElement(By.xpath("//span[@class='mail-MessagesSearchInfo-Title_misc nb-with-xs-left-gap']"))
                        .getText());

        logger.info("Writing message and send");
        driver.findElement(By.xpath("//a[@title='Написать (w, c)']")).click();
        wait.
                until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath("(//div[@class='compose-LabelRow'])[1]//div[@class='MultipleAddressesDesktop-Field ComposeYabblesField']/div")));
        driver.findElement(By.xpath("(//div[@class='compose-LabelRow'])[1]//div[@class='MultipleAddressesDesktop-Field ComposeYabblesField']/div"))
                .sendKeys("engovatovalex@yandex.ru");
        driver.findElement(By.xpath("//input[@class='composeTextField ComposeSubject-TextField']")).sendKeys("Simbersoft Тестовое задание Энговатов");
        driver.findElement(By.xpath("//div[@class='cke_wysiwyg_div cke_reset cke_enable_context_menu cke_editable cke_editable_themed cke_contents_ltr cke_htmlplaceholder']"))
                .sendKeys(result);
        driver.findElement(By.xpath("//button[@class='control button2 button2_view_default button2_tone_default button2_size_l button2_theme_action button2_pin_circle-circle ComposeControlPanelButton-Button ComposeControlPanelButton-Button_action']")).click();
        logger.info("Test passed");
    }
}
